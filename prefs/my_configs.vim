set noshowmode

let g:lightline.colorscheme='powerline'

inoremap jk <esc>
inoremap <esc> <nop>
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>
