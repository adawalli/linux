#!/bin/bash

command_exists () {
    type "$1" &> /dev/null ;
}

#fix timezone
sudo ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
sudo dpkg-reconfigure -f noninteractive tzdata

sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get install -y --no-install-recommends gnome-terminal

sudo apt-get install -y \
    build-essential \
    curl \
    git \
    python-pip \
    python3-pip \
    fonts-inconsolata \
    apt-transport-https \
    ca-certificates \
    gnupg \
    software-properties-common \
    vim-nox \
    silversearcher-ag

if ! command_exists code ; then
    curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
    sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
    sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
    sudo apt-get update
        sudo apt-get install -y code
    userCode="$HOME/.config/Code/User"
    code -s &>/dev/null
    while [[ ! -e $userCode ]] ; do sleep 1 ; done
    settingsUser=$userCode/settings.json
    cp prefs/code_settings.json $settingsUser
fi

if [[ ! -e ~/.bashit ]]; then
    git clone https://github.com/Bash-it/bash-it.git ~/.bashit
    #git clone https://github.com/adawalli/bash-it.git ~/.bashit
    (cd ~/.bashit && echo "N" | . ~/.bashit/install.sh)
    sed -i 's/bobby/powerline-multiline/g' ~/.bashrc
fi

if [[ ! -e ~/.gitconfig ]]; then
    git config --global user.email "adam.wallis@gmail.com"
    git config --global user.name "Adam Wallis"
fi

FONT_HOME=~/.local/share/fonts

if [[ ! -e $FONT_HOME/adobe-fonts/source-code-pro ]]; then
    echo "installing fonts at $PWD to $FONT_HOME"
    mkdir -p "$FONT_HOME/adobe-fonts/source-code-pro"
    # find "$FONT_HOME" -iname '*.ttf' -exec echo '{}' \;

    (git clone \
    --branch release \
    --depth 1 \
    'https://github.com/adobe-fonts/source-code-pro.git' \
    "$FONT_HOME/adobe-fonts/source-code-pro" && \
    fc-cache -f -v "$FONT_HOME/adobe-fonts/source-code-pro")
fi

cp prefs/gitconfig $HOME/.gitconfig

git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
(. ~/.vim_runtime/install_awesome_vimrc.sh)

cp ./prefs/my_configs.vim ~/.vim_runtime/
git clone https://github.com/tpope/vim-git.git $HOME/.vim_runtime/my_plugins/vim-git
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
source ./uninstall.sh
source ./install.sh
cd ..
rm -rf fonts

if ! command_exists docker ; then
    curl -fsSL get.docker.com -o /tmp/get-docker.sh
    sudo sh /tmp/get-docker.sh
    me="$(whoami)"
    sudo usermod -aG docker $me
fi
sudo pip install virtualenv virtualenvwrapper
sudo pip3 install powerline-status

# Gotta have fzf!
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --key-bindings --completion --no-update-rc
# end fzf

# Install JUMP
curl -Lfo jump.deb https://github.com/gsamokovarov/jump/releases/download/v0.19.0/jump_0.19.0_amd64.deb
sudo dpkg -i jump.deb
rm jump.deb
# end jump

# Install FD
curl -Lfo fd.deb https://github.com/sharkdp/fd/releases/download/v7.0.0/fd_7.0.0_amd64.deb
sudo dpkg -i fd.deb
rm fd.deb
# end fd

. ~/.bashrc
bash-it enable plugin git docker virtualenv powerline jump fzf
bash-it enable alias git docker
bash-it enable completion git docker

# Restore Gnome Terminal Settings
dconf reset -f /org/gnome/terminal/
dconf load /org/gnome/terminal/ < ./prefs/gnome_terminal_settings_backup.txt

